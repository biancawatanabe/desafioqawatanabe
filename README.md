# Desafio Conductor de Seleção QA


# Plano de Teste para o Valida Processamento v1.0

# 1. Objetivos

## 1.1. Objetivo

Este documento descreve o plano para testar o protótipo do sistema de processamentos de
Valida Processamento 1.0. Este documento de Plano de Teste suporta os seguintes
objetivos:

Identificar informações existentes do projeto e o software que deve ser testado.
Listar os requisitos de teste recomendados.

## 1.2. Escopo

Este Plano de Teste descreve os testes de unidade e do sistema que serão conduzidos no Valida Processamento 1.0. 

É crítico que todas as interfaces do sistema e do subsistema sejam testadas, bem como o
desempenho do sistema nesse estágio antecipado.

As interfaces entre os seguintes subsistemas serão testadas:

* Login - Passed;
* Cadastrar cliente  - Passed;
* Consultar cliente  - Passed;
* Realizar venda  - Error;
* Consultar venda  - Passed;

As medidas de desempenho mais críticas a testar são:

* Tempo de resposta para login
* Tempo de resposta para acesso ao Valida Processamento 1.0.
* Tempo de resposta para listagem de transações.
* Tempo de resposta para inserção de clientes no sistema.
* Tempo de resposta para listagem de clientes.

# 2. Requisitos para o Teste

A lista a seguir identifica os itens (casos de uso, requisitos funcionais, requisitos não
funcionais que foram identificados como alvos do teste. Essa lista representa o que será
testado.

* Inserção e listagem de clientes.
* Inserção e listagem de transações.
* API

### 2.1. Teste de Interface com o Usuário

Verificar a facilidade de navegação utilizando um conjunto de amostras de telas.

### 2.2. Teste de Desempenho

* Verificar o tempo de resposta para acesso ao sistema.
* Verificar o tempo de resposta para acesso a transações.
* Verificar o tempo de resposta para login.
* Verificar o tempo de resposta para acesso a lista de clientes.

### 2.3. Teste de Carga

Verificar a resposta do sistema quando estiver carregado com 200 clientes com logon efetuado.

### 2.4. Teste de Estresse

Nenhum.

### 2.5. Teste de Volume

Nenhum.

### 2.6. Teste de Regras de Negócio

Verificar se aplicação respeita as regras de negócio 

Para realizar uma venda, apenas será permitido caso o saldo do cliente for igual ou superior ao valor da compra.

Realizado teste na página de transação foi constatado que regra de negócio não está sendo obedecida pois após inserção de valor do saldo inferior ao valor da compra foi aprovada com sucesso, sendo assim qualquer cliente conseguiria facilmente comprar produtos mesmo que não haja saldo.

### 2.7. Teste de Configuração

Nenhum.

### 2.8. Teste de Instalação

Nenhum.

### 2.9. Teste de Função
Os testes do aplicativo devem ter foco em quaisquer requisitos de destino que possam ser rastreados diretamente para casos de uso (ou funções de negócios) e regras de negócios e implementação apropriada das regras de negócios.

Objetivo do Teste: Assegurar a navegação correta do aplicativo.

### 2.10. Teste de Failover e Recuperação

Esta seção não é aplicável ao teste do software em questão.

# 3. Ferramentas

As seguintes ferramentas serão empregadas para o teste do protótipo

* Postman

# 4 Sistema e Repositório

 BitBucket  - Repositório de Testes, aplicação para teste de API, back-end.

# 5.Conjunto de Teste

O Conjunto de Teste definirá todos os casos de teste e os scripts de teste associados a
cada caso de teste aplicados pelo QA.




## Cenário de teste ##

* Verificar lista de clientes

* Verificar vendas processadas 

* Verificar clientes ativos e inativos 

* Verificar se ao fechar aba desloga automaticamente 

* Não desloga automaticamente, é necessário fazer logout.

## Caso de teste ##

* Verificar se a inserção de clientes está constando automaticamente na listagem respectiva.

* Verificar se a inserção de transação está constando automaticamente na listagem respectiva.

* Verificar input de campos com tipo de variável correto
	
	*Campo “nome” aceita caracteres do tipo número.

* Verificar se a aplicação atende às regras de negócio. 
   
**Nota:** *Realizado teste na página de transação foi constatado que regra de negócio não está sendo obedecida pois após inserção de valor do saldo inferior ao valor da compra foi aprovada com sucesso.